<?php get_header(); ?>

<div class="container-content page-frame">

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>

    <?php endwhile; ?>

<?php endif; ?>
<?php wp_reset_postdata(); ?>

</div>

<?php get_footer(); ?>

