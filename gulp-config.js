module.exports = {
	'browserSync': {
		notify                 : false,
		proxy                  : '<awsitename>.dev',
		port                   : 3000,
		tunnel                 : null,
		scrollRestoreTechnique : 'cookie'
	}
}

