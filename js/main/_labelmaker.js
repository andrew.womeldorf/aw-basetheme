(function ($) {

	$.fn.labelMaker = function( options ){
    	var opts = $.extend( {}, $.fn.labelMaker.defaults, options );

    	return this.each(function(){

            var labelMaker = {
                l: $(this),

				setForEl: function(){
                    this.forEl = $('*[id="'+this.l.attr('for')+'"]');
				},

				focuser: function(point){
					var a = {};
					$.each(opts.animations, function(index){
						a[opts.animations[index][0]] = opts.animations[index][1][point];
					});
					this.l.css(a);
				},

                setUp: function(){
					this.focuser(0);

					var transitionProperties = '';
					$.each(opts.animations, function(index){
						if ( index === 0 ) {
							transitionProperties += opts.animations[index][0];
						}
						else{
							transitionProperties += (', ' + opts.animations[index][0]);
						}
					});

					this.l.siblings().css('position', 'relative');

                    this.l.css({
						'position': 'absolute',
						'-webkit-transition-property': transitionProperties,
                        '-moz-transition-property': transitionProperties,
                        '-ms-transition-property': transitionProperties,
                        '-o-transition-property': transitionProperties,
                        'transition-property': transitionProperties,
						'-webkit-transition-duration': opts.animationDuration + 's',
                        '-moz-transition-duration': opts.animationDuration + 's',
                        '-ms-transition-duration': opts.animationDuration + 's',
                        '-o-transition-duration': opts.animationDuration + 's',
                        'transition-duration': opts.animationDuration + 's',
						'-webkit-transition-timing-function': opts.easeFunction + 's',
                        '-moz-transition-timing-function': opts.easeFunction + 's',
                        '-ms-transition-timing-function': opts.easeFunction + 's',
                        '-o-transition-timing-function': opts.easeFunction + 's',
                        'transition-timing-function': opts.easeFunction + 's'
					});

					if ( opts.overflowHidden ) {
						this.l.parent().css('overflow','hidden');
					}
                }
            };

			labelMaker.setForEl();

        	if ( !labelMaker.forEl.is('[type="checkbox"]') && !labelMaker.forEl.is('[type="radio"]') && labelMaker.l.attr('for') ) {
        		labelMaker.setUp();
	            labelMaker.l.on('click', function(){
					labelMaker.focuser(0);
	            });
	            labelMaker.forEl.on('click change keydown', function(){
					if ( $(this).val() || $(this).is(':focus') ) {
						labelMaker.focuser(1);
					}
					else{
						labelMaker.focuser(0);
					}
	            });
				$(window).load(function(){
					if ( labelMaker.forEl.val() || labelMaker.forEl.is(':focus') ) {
						labelMaker.focuser(1);
					}
					else{
						labelMaker.focuser(0);
					}
	            });
				$(document).on('click keyup', function(e){
					if ( !labelMaker.l.parent().is(e.target) && labelMaker.l.parent().has(e.target).length === 0 ) {
						if ( !labelMaker.forEl.is(':focus') && !labelMaker.forEl.val() ) {
							labelMaker.focuser(0);
						}
					}
					if ( e.keyCode === 9 && labelMaker.forEl.is(':focus') || labelMaker.forEl.val() ) {
						labelMaker.focuser(1);
					}
				});
        	}
        });
	};

	// Plugin defaults - added as a property on labelMaker function
	$.fn.labelMaker.defaults = {
        animations: [
			['opacity', [1, 0]]
		],
		overflowHidden: true,
        animationDuration: 0,
		easeFunction: 'ease-in-out'
	};

}(jQuery));

(function($) {

	$( '.theme-header label, .support__search label' ).labelMaker({
		animations: [
			['opacity', [1, 0]],
			['z-index', [20, -1]]
		],
		overflowHidden   : false,
		animationDuration: 0.25
	});

	$( '#map-search label' ).labelMaker({
        animations: [
			['font-size', [16, 12]],
			['line-height', ['72px', '15px']]
		],
		overflowHidden   : false,
		animationDuration: 0.25
	});

}(jQuery));

