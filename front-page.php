<?php get_header(); ?>

<?php $file_name = get_file_name(__FILE__); ?>

<?php 
$file_name = get_file_name(__FILE__);
$directory = realpath(__DIR__) . '/';
?>

<?php 
if( have_rows( 'layouts' ) ):
    while ( have_rows( 'layouts' ) ) : the_row();

        $section_name = get_row_layout();
        if ( file_exists($directory .  'layouts/' . $file_name . '/' . $section_name . '.php') ) {
            get_template_part( 'layouts/' . $file_name . '/' . $section_name );
        } elseif ( file_exists( $directory . 'layouts/global/' . $section_name . '.php' ) ) {
            get_template_part( 'layouts/global/' . $section_name );
        }

    endwhile;
endif;
?>

<?php get_footer(); ?>

