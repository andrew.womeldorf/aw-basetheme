<?php

//░░░░░░░░░░░░░░░░░░░░░░░░
//
//     DIRECTORY
//
//     _Includes
//     _GeneralSettings
//     _Styles
//     _LoginStyles
//     _AdminStyles
//     _Javascript
//     _AdminJavascript
//     _Fonts
//     _Menus
//     _AddOptions
// 	   _AddImageSizes
// 	   _jQuery
//
//░░░░░░░░░░░░░░░░░░░░░░░░

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_Includes≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
include('includes/<awprefix>_general_wp_functions.php');
include('includes/<awprefix>_custom_post_types.php');

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_GeneralSettings≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
add_theme_support( 'menus' );
add_theme_support( 'html5' );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_Styles≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
function <awprefix>_theme_styles() {
	wp_enqueue_style( 'main_css', get_asset( 'main.min.css', 'css' ) );
}
add_action( 'wp_enqueue_scripts', '<awprefix>_theme_styles' );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_LoginStyles≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
function <awprefix>_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_asset('login.min.css','css') );
}
add_action( 'login_enqueue_scripts', '<awprefix>_login_stylesheet' );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_AdminStyles≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
function <awprefix>_admin_stylesheet() {
	wp_enqueue_style( 'custom-admin', get_asset('admin.min.css','css') );
}
add_action( 'admin_enqueue_scripts', '<awprefix>_admin_stylesheet' );

function <awprefix>_fontawesome_dashicons(){
    wp_enqueue_style( 'fontawesome', get_asset( 'fa-dashicons.min.css', 'font-awesome/css' ) );
}
add_action( 'admin_init', '<awprefix>_fontawesome_dashicons' );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_Javascript≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
function <awprefix>_theme_js() {
	global $wp_scripts;
	wp_register_script( 'html5_shiv', 'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js', '', '', false );
	$wp_scripts->add_data( 'html5_shiv', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'functions', get_asset( 'main.min.js', 'js' ), array('jquery'), '', true );
    wp_localize_script( 'functions', 'OPTIONS', array(
        'home_url'                => preg_replace( '/https?:\/\//', '', get_home_url() ),
        'ajax_url'                => admin_url( 'admin-ajax.php' ),
        'ajax_nonce'              => wp_create_nonce( 'ajax_nonce' )
    ) );
}
add_action( 'wp_enqueue_scripts', '<awprefix>_theme_js' );

// Async & Defer on selected scripts
function <awprefix>_add_async_and_defer( $tag, $handle ){
    $scripts_to_async_and_defer = array(
        'google_maps'
    );

    foreach( $scripts_to_async_and_defer as $async_and_defer_script ){
        if( $async_and_defer_script === $handle ){
            return str_replace( ' src', ' async="async" defer="defer" src', $tag );
        }
    }

    return $tag;
}
//add_filter( 'script_loader_tag', '<awprefix>_add_async_and_defer', 10, 2 );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_AdminJavascript≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
function <awprefix>_admin_js(){
    $screen = get_current_screen();
    if( $screen->id === 'edit-pharmacy' ){
        wp_enqueue_script( 'adminjs', get_asset( 'admin.min.js', 'js' ), array('jquery'), '', true );
        wp_localize_script( 'adminjs', 'OPTIONS', array(
            'url'         => admin_url( 'admin-ajax.php' ),
            'ajax_nonce'  => wp_create_nonce( 'ajax_nonce' )
        ));
    }
}
add_action( 'admin_enqueue_scripts', '<awprefix>_admin_js' );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_Fonts≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
// function <awprefix>_theme_fonts() {
//    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/assets/fonts/font-awesome/css/font-awesome.min.css');
// }
// add_action('wp_print_styles', '<awprefix>_theme_fonts');

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_Menus≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
function <awprefix>_register_theme_menus() {
	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu' ),
			'footer-menu' => __( 'Footer Menu' ),
		)
	);
}
add_action( 'init', '<awprefix>_register_theme_menus' );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_AddOptions≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
if( function_exists('acf_add_options_page') ) {

    $parent = acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug'  => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false
    ));

    //acf_add_options_sub_page(array(
    //    'page_title'  => 'Social Settings',
    //    'parent_slug' => $parent['menu_slug'],
    //    'post_id' => 'social',
    //));

}

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_AddImageSizes≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
//function <awprefix>_wpdocs_theme_setup() {
//    add_image_size( 'cover', 1400, 850, false );
//}
//add_action( 'after_setup_theme', '<awprefix>_wpdocs_theme_setup' );

add_theme_support( 'post-thumbnails' );

//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡_jQuery≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
/** modify_jquery / add_action( init )
 * Change out native WP jQuery file for our own
 *
 * When used, shares unminified jQuery file for debugging
 */
function <awprefix>_modify_jquery(){
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_asset( 'jquery.js', 'dev' ));
    wp_enqueue_script('jquery');
}
//add_action('init', '<awprefix>_modify_jquery');
