<?php

function add_post_types(){
    register_post_type( '', array(
        'labels'                => array(
            'name'               => '',
            'singular_name'      => '',
            'all_items'          => '',
            'new_item'           => '',
            'add_new'            => '',
            'add_new_item'       => '',
            'edit_item'          => '',
            'view_item'          => '',
            'search_items'       => '',
            'not_found'          => '',
            'not_found_in_trash' => '',
            'parent_item_colon'  => '',
            'menu_name'          => '',
        ),
        'public'                => true,
        'hierarchical'          => false,
        'supports'              => array( 'title', 'thumbnail' ),
        'has_archive'           => true,
        'rewrite'               => true,
        'query_var'             => true,
        'menu_icon'             => 'dashicons-fa-heartbeat',
    ));
}
add_action( 'init', 'add_post_types' );
