# AW-BASETHEME

Wordpress Development theme that I'm most familiar with.  Has all the core functions I like, and is set up for gulp and browsersync.

This is used by a script which replaces all of the `aw` prefixes in filenames and the folder name, as well as the `<awprefix>` in the files.
If you clone this outside of that script, change into this directory and replace all `<awprefix>` with this:
```
perl -pi -e 's/<awprefix>/yourprefix/g' *
```
The above also applies to `<awsitename>`

Also, remember to udpate style.css, particularly the author field.
